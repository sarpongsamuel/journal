<head>
    <title>JAFS | @yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('home/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('home/css/animate.css')}}">
    
    <link rel="stylesheet" href="{{asset('home/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('home/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('home/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('home/css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('home/css/ionicons.min.css')}}">
    
    <link rel="stylesheet" href="{{asset('home/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('home/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('home/css/style.css')}}">

    {{-- fontawesome linking --}}
    <link rel="stylesheet" href="{{asset('home/fontawesome/css/all.css')}}">
  </head>