<nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container d-flex align-items-center px-4">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
      </button>
      <form action="#" class="searchform order-lg-last">
      <div class="form-group d-flex">
        <input type="text" class="form-control pl-3" placeholder="Article Title, Author, Keywords etc.">
        <button type="submit" placeholder="" class="form-control search"><span class="ion-ios-search"></span></button>
      </div>
    </form>
      <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav mr-auto">
        <li class="nav-item"><a href="{{route('pages.index')}}" class="nav-link pl-0">HOME</a></li>
            <li class="nav-item"><a href="{{route('pages.instructions')}}" class="nav-link">INSTRUCTION TO AUTHORS</a></li>
            <!-- <li class="nav-item"><a href="courses.html" class="nav-link">EDITORIAL BOARD</a></li> -->
            <li class="nav-item"><a href="{{route('pages.editorial_board')}}" class="nav-link">EDITORIAL BOARD</a></li>
            <!-- <li class="nav-item"><a href="blog.html" class="nav-link">Blog</a></li> -->
          <li class="nav-item"><a href="{{route('pages.contact')}}" class="nav-link">CONTACT US</a></li>
        </ul>
      </div>
    </div>
</nav>