<!DOCTYPE html>
<html lang="en">
    @include('partials._head')
  <body>
    @include('partials._header') 
	@include('partials._nav')
	<!-- END nav -->   
    <section class="ftco-section ftco-no-pt ftc-no-pb">
			<div class="container">
				<div class="row d-flex">
					<div class="col-md-3">
                            <div class="" style="margin-top: 15px; margin-bottom: 20px" >
                                    <table class="table table-bordered" style="text-align: left">
                                      <tbody>

                                        <tr>
                                          <td style="font-display: bold">
                                                  <a class="nav-link collapsed" href="#submenu1" data-toggle="collapse" data-target="#submenu1">
                                                      <span class="fas fa-plus-circle"></span>
                                                    JAFS Pubs. 2019 
                                                    <span class="fas fa-caret-down"></span></a>
                                                  <div class="collapse" id="submenu1" aria-expanded="false">
                                                    <div class="justify-content-cente badge-box">
                                                      <a href="#" class="box-text"> <span class="badge badge-danger">1</span></a>
                                                      <a href="#"> <span class="badge badge-danger">2</span></a>
                                                      <a href="#"> <span class="badge badge-danger">3</span></a>
                                                      <a href="#"> <span class="badge badge-danger">4</span></a>
                                                      <a href="#"> <span class="badge badge-danger">5</span></a>
                                                    </div>
                                                  </div>
                                                </td>
                                              </tr>

                                      </tbody>
                                    </table>
                                    
                                  </div>
                    @yield('sidebar')
                    </div>
                    
                    <div class="col-md-9">
                        @yield('content')
                    </div>
                </div>
            </div>
    </section>


@include('partials._footer')

  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

  @include('partials._scripts')
 
  </body>
</html>