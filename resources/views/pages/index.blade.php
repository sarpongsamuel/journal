@extends('layouts.main')
@section('title' ,'Home')
@section('content')
<section class="ftco-section testimony-section">
        <div class="row">
         <div class="container">
           <div class="row ftco-animate justify-content-center">
             <div class="col-md-12">
               <div class="carousel-testimony owl-carousel">
                 <div class="item">
                   <div class="testimony-wrap d-flex">
                     <div class="user-img mr-4" style="background-image: url(images/teacher-1.jpg)">
                     </div>
                     <div class="text ml-2">
                         <span class="quote d-flex align-items-center justify-content-center">
                         <i class="icon-quote-left"></i>
                       </span>
                       <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                       <p class="name">Racky Henderson</p>
                       <span class="position">Father</span>
                     </div>
                   </div>
                 </div>
                 <div class="item">
                   <div class="testimony-wrap d-flex">
                     <div class="user-img mr-4" style="background-image: url(images/teacher-2.jpg)">
                     </div>
                     <div class="text ml-2">
                         <span class="quote d-flex align-items-center justify-content-center">
                         <i class="icon-quote-left"></i>
                       </span>
                       <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                       <p class="name">Henry Dee</p>
                       <span class="position">Mother</span>
                     </div>
                   </div>
                 </div>
                 <div class="item">
                   <div class="testimony-wrap d-flex">
                     <div class="user-img mr-4" style="background-image: url(images/teacher-1.jpg)">
                     </div>
                     <div class="text ml-2">
                         <span class="quote d-flex align-items-center justify-content-center">
                         <i class="icon-quote-left"></i>
                       </span>
                       <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                       <p class="name">Ken Bosh</p>
                       <span class="position">Mother</span>
                     </div>
                   </div>
                 </div>
               </div>
             </div>
           </div>
           </div>
         </div>
       </section>

       {{-- display journal here --}}
       <section class="ftco-section ">
			<div class="container px-3" style="margin-top:-90px">
				<div class="row">
					<a>
					<div class="col-md-6 col-lg-6 ftco-animate" >
						<div class="card text-white bg-secondary mb-3" style="" id="card">
								<div class="card-header">Original Article</div>
							<div class="card-body">
								<div class="media">
                <a href="{{route('pages.journal',1)}}"><img class="align-self-start mr-3" src="{{asset('home/images/pdf.jpg')}}" style="height:70px; width:90px"></a>

										<div class="media-body">

										<p class="card-text">
											<a href=""> header here.</a>
										</p>
										</div>
								</div>  
									<p class="card-text"><small class="text-muted">Samuel, awuah , sarpong</small></p>
									<p class="card-text" style="margin-top:-14px">
										<a href=""><small class="text-muted">https://www.hey.com</small></a>
									</p>
							</div>
								<div class="card-footer">Aug 2019(vol 1 n1)</div>
						</div>
					</div>
				</a>
					<div class="col-md-6 col-lg-6 ftco-animate" >
							<div class="card text-white bg-secondary mb-3" style="" id="card">
									<div class="card-header">Original Article</div>

								<div class="card-body">
									<div class="media">
										<a href=""><img class="align-self-start mr-3" src="{{asset('home/images/pdf.jpg')}}" style="height:70px; width:90px"></a>
											<div class="media-body">
											<p class="card-text">
												<a href=""> header here.</a>
											</p>
											</div>
									</div>  
										<p class="card-text"><small class="text-muted">Samuel, awuah , sarpong</small></p>
										<p class="card-text" style="margin-top:-14px">
											<a href=""><small class="text-muted">https://www.hey.com</small></a>
										</p>
								</div>
									<div class="card-footer">
										Reviewed
									</div>
							</div>
						</div>
					
				</div>
			</div>
		</section>
@endsection