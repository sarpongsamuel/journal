<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    //

    public function index(){
        return view('pages.index');
    }

    public function instructions(){
        return view('pages.instructions');
    }

    public function editorial_board(){
        return view('pages.editorial_board');
    }

    public function contact(){
        return view('pages.contact');
    }

    public function journal($id){
        return view('pages.Journal');
    }
}
