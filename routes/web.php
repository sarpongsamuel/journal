<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'PagesController@index')->name('pages.index');
Route::get('/instruction', 'PagesController@instructions')->name('pages.instructions');
Route::get('/editorial_board', 'PagesController@editorial_board')->name('pages.editorial_board');
Route::get('/contact', 'PagesController@contact')->name('pages.contact');
Route::get('/journal/{id}', 'PagesController@journal')->name('pages.journal');

